-- :name get-message-events :?
-- :doc Get sorted non-rejected message events by sender like, :sender-like should include % wildcards

SELECT e.event_id, e.stream_ordering, e.origin_server_ts, j.json
FROM events e
       LEFT OUTER JOIN rejections r USING (event_id)
       JOIN event_json j USING (event_id)
WHERE r.event_id IS NULL
  AND e.type IN ('m.room.message', 'm.room.encrypted')
  AND e.sender ILIKE :sender-like
  AND e.origin_server_ts >= :ts-lower
  AND e.origin_server_ts < :ts-upper
ORDER BY e.stream_ordering ASC, e.origin_server_ts ASC
LIMIT :limit OFFSET :offset

-- :name count-message-events :1
-- :doc Count the message events, see get-message-events

SELECT count(e.event_id)
FROM events e
       LEFT OUTER JOIN rejections r USING (event_id)
       JOIN event_json j USING (event_id)
WHERE r.event_id IS NULL
  AND e.type IN ('m.room.message', 'm.room.encrypted')
  AND e.sender ILIKE :sender-like
  AND e.origin_server_ts >= :ts-lower
  AND e.origin_server_ts < :ts-upper


-- :name lookup-device-name :1
-- :doc Get the device name for a device given its device_id
SELECT * FROM devices WHERE device_id = :device-id;

-- :name lookup-devices :?
-- :doc Get devices for a user

SELECT d.user_id,
d.device_id,
d.display_name,
i.ip as last_seen_ip,
i.user_agent,
i.last_seen as last_seen_ts
FROM devices d
INNER JOIN user_ips i ON d.device_id = i.device_id
WHERE d.user_id = :user-id;

-- :name _count-joined-members :1
-- _doc Get the current number of joined users in a room
SELECT count(*) FROM room_memberships as m
INNER JOIN current_state_events as c USING(event_id)
WHERE c.type = 'm.room.member' AND c.room_id = :room-id
AND membership = 'join' GROUP BY m.membership;
