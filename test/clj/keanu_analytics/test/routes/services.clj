(ns keanu-analytics.test.routes.services
  (:require

   [clojure.test :refer :all]
   [ring.mock.request :refer :all]
   [muuntaja.core :as m]
   [mount.core :as mount]

   [cheshire.core :as json]
   [keanu-analytics.handler :refer :all]
   [keanu-analytics.middleware.formats :as formats]
   [keanu-analytics.routes.services :as l]))


(deftest test-take-ip
  (testing "takeup"
    (let [devices [
                   {:last_seen_ts 0 :last_seen_ip "120"}
                   {:last_seen_ts 1 :last_seen_ip "123"}
                   {:last_seen_ts 2 :last_seen_ip nil}
                   {:last_seen_ts 3 :last_seen_ip "-"}
                   ]]

      (is (= "123" (:last_seen_ip (l/get-valid-last-seen-ip  devices))))
      (is (= nil (l/get-valid-last-seen-ip nil)))
      (is (= nil (l/get-valid-last-seen-ip {})))
      (is (= nil (l/get-valid-last-seen-ip [])))
      (is (= nil (l/get-valid-last-seen-ip [{}])))
      (is (= nil (l/get-valid-last-seen-ip [(last devices)]))))))

(def test-web-domain "web.client.example.com")
(deftest client-classification
  (let [test-domains [test-web-domain "test2.com"]
        test-regexes [[#"FooApp-[a-zA-Z0-9]{8}$" {:platform "ios" :appname "FooApp"}]
                      [#"Keanu-[a-zA-Z0-9]{8}$"  {:platform "ios" :appname "Keanu"}]]

        test-prefixes [["device-android-" {:platform "android" :appname "FooApp"}]
                       ["device-" {:platform "android" :appname "FooApp"}]]]
    (testing "keanu-device-name-standard-parsing"
      (are [ua res] (= res (l/parse-device-name ua))
        "Keanu-android" {:platform "android" :appname "Keanu"}
        "Circles-android" {:platform "android" :appname "Circles"}
        "Keanu-android-ANBCDE01" {:platform "android" :appname "Keanu"}
        "Keanu-android-" nil
        "Foo-ios-ABCDE" {:platform "ios" :appname "Foo"}
        "foo-ios" {:platform "ios" :appname "foo"}))

    (testing "web-client-name-parsing"
      (are [ua] (l/is-web-client? ua test-web-domain)
        "https://web.client.example.com/ via Chrome on Android"
        "在 Windows 下 Chrome 浏览器中运行的 https://web.client.example.com/")
      (are [ua res] (= res (l/parse-web-client ua test-web-domain))
        "https://web.client.example.com/ via Chrome on Android" {:platform "android" :appname test-web-domain}
        "https://web.client.example.com/ via Chrome on Linux" {:platform "linux" :appname test-web-domain}
        "https://web.client.example.com/ via Chrome on Mac OS" {:platform "macos" :appname test-web-domain}
        "https://web.client.example.com/ via Chrome on Windows" {:platform "windows" :appname test-web-domain}
        "https://web.client.example.com/ via Chrome på Android" {:platform "android" :appname test-web-domain}
        "https://web.client.example.com/ via Firefox on Fedora" {:platform "linux" :appname test-web-domain}
        "https://web.client.example.com/ via Firefox on Linux" {:platform "linux" :appname test-web-domain}
        "https://web.client.example.com/ via Firefox on Mac OS" {:platform "macos" :appname test-web-domain}
        "https://web.client.example.com/ via Firefox on Ubuntu" {:platform "linux" :appname test-web-domain}
        "https://web.client.example.com/ via Firefox on Windows" {:platform "windows" :appname test-web-domain}
        "https://web.client.example.com/ via Mobile Safari on iOS" {:platform "ios" :appname test-web-domain}
        "https://web.client.example.com/ via Safari on Mac OS" {:platform "macos" :appname test-web-domain}
        "https://web.client.example.com/ с Chrome на Linux" {:platform "linux" :appname test-web-domain}
        "在 Windows 下 Chrome 浏览器中运行的 https://web.client.example.com/" {:platform "windows" :appname test-web-domain}))

    (testing "unified-device-name-parsing"
      (are [ua res] (= res (l/classify-device-name
                            test-prefixes test-regexes test-domains
                            ua))
        "https://web.client.example.com/ via Firefox on Ubuntu" {:platform "linux" :appname test-web-domain}
        "在 Windows 下 Chrome 浏览器中运行的 https://web.client.example.com/" {:platform "windows" :appname test-web-domain}
        "device-android-ABCD" {:platform "android" :appname "FooApp"}
        "device-ABCD" {:platform "android" :appname "FooApp"}
        "FooApp-ABcdEF21" {:platform "ios" :appname "FooApp"}
        "Keanu-ABCDefGH" {:platform "ios" :appname "Keanu"}
        "Keanu-android" {:platform "android" :appname "Keanu"}
        "Circles-android" {:platform "android" :appname "Circles"}
        "Keanu-android-ANBCDE01" {:platform "android" :appname "Keanu"}
        "Keanu-android-" {:platform "unknown" :appname "unknown"}
        "Foo-ios-ABCDE" {:platform "ios" :appname "Foo"}
        "foo-ios" {:platform "ios" :appname "foo"}))))

(comment
  (defn slurp-json [f]
    (-> f
        slurp
        (json/parse-string true)))

  (def test-event (slurp-json "test/fixtures/event.json"))

  (deftest test-funcs
    (testing "foo"
      (let [events (:events  test-event)
            event (first events)]
        (is (= ["@example" "matrix.org"]
               (l/split-mxid (:user_id event))))
        (is (l/sender-is-own-user? event "matrix.org")))))

  (use-fixtures
    :once
    (fn [f]
      (mount/start #'keanu-analytics.config/env
                   #'keanu-analytics.handler/app
                   #'keanu-analytics.handler/app-routes)
      (f)))

  (deftest test-app
    (testing "main route"
      (let [response ((app) (request :get "/"))]
        (is (= 301 (:status response)))))

    (testing "not-found route"
      (let [response ((app) (request :get "/invalid"))]
        (is (= 404 (:status response)))))
    (testing "services"

      (testing "transaction"
        (let [response ((app) (-> (request :put "/_matrix/app/v1/transactions/123")
                                  (json-body test-event)))]
          (is (= 200 (:status response))))))))
