FROM openjdk:8-alpine

COPY target/uberjar/keanu-analytics.jar /keanu-analytics/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/keanu-analytics/app.jar"]
