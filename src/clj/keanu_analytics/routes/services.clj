(ns keanu-analytics.routes.services
  (:require
   [cheshire.core :as json]
   [keanu-analytics.cal :as cal]
   [keanu-analytics.bloom :as bloom]
   [keanu-analytics.matrix-api :as api]
   [keanu-analytics.config :refer [env]]
   [keanu-analytics.routes.ip-geo :as ipgeo]
   [keanu-analytics.db.core :as db]
   [keanu-analytics.syndb :as syndb]
   [keanu-analytics.memdb :as memdb]
   [conman.core :refer [with-transaction]]
   [clj-http.client :as http]
   [clojure.string :as string]
   [clojure.core.memoize :as memo]
   [clojure.tools.logging :as log]))

(defn in?
  "Retturns true if coll contains elm."
  [coll elm]
  (some #(= elm %) coll))


(defn split-mxid [mxid]
  (string/split mxid #":" 2))

(defn is-federated-event?
  "Returns true if the sender or room id belong to a server that isn't own-hs"
  ([mevent own-hs]
   (let [[user sender-hs]  (split-mxid (:sender mevent))
         [roomid room-hs]  (split-mxid (:room_id mevent))]
     (or
      (not= sender-hs own-hs)
      (not= room-hs own-hs)))))

(defn sender-is-own-user?
  "Returns true if the mxid of the event belongs to our homeserver"
  ([event]
   (sender-is-own-user? event (:hs-url env)))
  ([event own-hs]
   (let [[user hs]  (split-mxid (:sender event))]
     (= hs own-hs))))

(defn- get-recent-user-ip-api! [mxid]
  (let [devices (:devices (api/get-devices api/matrix-client mxid))
        last-seen-device (last (sort-by :last_seen_ts devices))
        last-seen-ip (:last_seen_ip last-seen-device)]
    last-seen-ip))

(def count-room-members-api
  ;; cache the count joined members responses for several seconds
  (memo/ttl api/count-joined-members :ttl/threshold 3000))

(defn get-valid-last-seen-ip [devices]
  (:last_seen_ip (->> devices
       (sort-by :last_seen_ts #(compare %2 %1))
       (some
        #(let [ip (:last_seen_ip %)] (when  (and  (some? ip) (not= "-" ip)) %))))))

(defn- _get-recent-user-ip! [mxid]
  (get-valid-last-seen-ip (syndb/get-devices mxid)))

(def get-recent-user-ip!
  (memo/ttl _get-recent-user-ip! :ttl/threshold 60000))

(def count-room-members
  ;; cache the count joined members responses for several 1 minute
  (memo/ttl syndb/count-joined-members :ttl/threshold 60000))

(def get-device-name
  (memoize syndb/get-device-name))

(defn clear-memos []
(memo/memo-clear! get-recent-user-ip!)
(memo/memo-clear! get-device-name)
(memo/memo-clear! count-room-members))

(defn is-room-group?-api [mevent]
  (> (count-room-members api/matrix-client (:sender mevent) (:room_id mevent))
     2))

(defn is-room-group? [mevent]
  (> (count-room-members (:room_id mevent))
     2))

(defn check-bloom-sender [type mevent]
  (let [mxid (:sender mevent)]
    (bloom/included? type mxid)))

(defn check-memdb-sender [type mevent]
  (let [mxid (:sender mevent)]
    (memdb/included? type mxid)))

(defn is-first-engagement? [mevent]
  (not (check-memdb-sender ::memdb/user-engage-first mevent)))

(defn is-period-engagement? [period mevent]
  (not (check-memdb-sender ::memdb/user-engage-period mevent)))

(defn is-group-engagement? [period mevent]
  (not (memdb/included? ::memdb/group-engage-period (:room_id mevent))))

(defn is-federated? [mevent]
  (is-federated-event? mevent (:hs-url env)))

(def re-device-name #"([a-zA-Z0-9]+)-(ios|android)(-[a-zA-Z0-9]+)?$")

(defn parse-device-name [device-name]
  (let [[_ appname platform _] (re-matches re-device-name device-name)]
    (when (and appname platform)
      {:platform platform :appname appname})))

(defn is-web-client? [device-name web-domain]
  (string/includes? device-name web-domain))

(defn parse-web-client [device-name web-domain]
  (when (is-web-client?  device-name web-domain)
    (cond
      (string/includes? device-name "Android") {:platform "android" :appname web-domain}
      (string/includes? device-name "Mac OS") {:platform "macos" :appname web-domain}
      (string/includes? device-name "Fedora") {:platform "linux" :appname web-domain}
      (string/includes? device-name "Linux") {:platform "linux" :appname web-domain}
      (string/includes? device-name "Ubuntu") {:platform "linux" :appname web-domain}
      (string/includes? device-name "iOS") {:platform "ios" :appname web-domain}
      (string/includes? device-name "Windows") {:platform "windows" :appname web-domain}
      :else {:platform "unknown" :appname web-domain})))

(defn parse-name-prefix [device-name [prefix result]]
  (when (string/starts-with? device-name prefix)
    result))

(defn parse-name-prefixes [prefixes device-name]
  (some (partial parse-name-prefix device-name) prefixes))

(defn parse-name-static-regex [device-name [pattern result]]
  (when (re-matches (re-pattern pattern) device-name)
    result))

(defn parse-name-static-regexes [regexes device-name]
  (some (partial parse-name-static-regex device-name) regexes))

(defn parse-web-clients [web-domains device-name]
  (some (partial parse-web-client device-name) web-domains))

(defn classify-device-name [prefixes regexes web-domains device-name]
  (or
    (when (nil? device-name) {:platform "unknown" :appname "unknown"})
   (parse-name-prefixes prefixes device-name)
   (parse-name-static-regexes regexes device-name)
   (parse-web-clients web-domains device-name)
   (parse-device-name device-name)
   {:platform "unknown" :appname "unknown"}))

(defn identify-client [mevent]
  (classify-device-name
   (:device-name-prefixes env)
   (:device-name-static-regexes env)
   (:web-client-domains env)
   (get-device-name (get-in mevent [:content :device_id]))))

(defn ->event [mevent]
  (let [mxid (:sender mevent)
        user-ip (get-recent-user-ip! mxid)
        resolver (ipgeo/ip-to-geo-bucket-resolver "other")
        period (cal/period-for (cal/ts->adate (:origin_server_ts mevent)))
        is-group (is-room-group? mevent)
        client (identify-client mevent)
        {platform :platform appname :appname} (identify-client mevent)]
    {:period period
     :event (:type mevent)
     :bucket (resolver user-ip)
     :is_user_engagement_period (is-period-engagement? period mevent)
     :is_user_engagement_first (is-first-engagement? mevent)
     :is_federated (is-federated? mevent)
     :is_group is-group
     :is_group_engagement_period (if is-group (is-group-engagement? period mevent) false)
     :platform platform
     :appname appname}))

(defn record-event! [event]
  (db/create-event! event))

(defn update-blooms! [event mevent]
  (when-let [roomid (:room_id mevent)]
    (when (:is_group_engagement_period event)
      (bloom/add-mxid! ::bloom/group-engage-period roomid)))

  (when-let [mxid (:sender mevent)]
    (when (:is_user_engagement_first event)
      (bloom/add-mxid! ::bloom/user-engage-first mxid))

    (when (:is_user_engagement_period event)
      (bloom/add-mxid! ::bloom/user-engage-period mxid))))

(defn update-memdb! [event mevent]
  (when-let [roomid (:room_id mevent)]
    (when (:is_group_engagement_period event)
      (memdb/add! ::memdb/group-engage-period roomid)))

  (when-let [mxid (:sender mevent)]
    (when (:is_user_engagement_first event)
      (memdb/add! ::memdb/user-engage-first mxid))

    (when (:is_user_engagement_period event)
      (memdb/add! ::memdb/user-engage-period mxid))))

(defn process-message-event!
  [mevent]
  (let [period (cal/period-for (cal/ts->adate (:origin_server_ts mevent)))]
    ;;(bloom/set-period! period)
    (memdb/set-period! period)
    (let [event (->event mevent)]
      ;; (with-transaction [db/*db*])
      (record-event! event)
      ;;(update-blooms! event mevent)
      (update-memdb! event mevent)

      )))

(defn process-unknown-event! [event]
  (println "unknown event" event))

(defn process-event! [event]
  (when (sender-is-own-user? event)
    (condp = (:type event)
      "m.room.message" (process-message-event! event)
      "m.room.encrypted" (process-message-event! event)
      (process-unknown-event! event))))

(defn receive-transaction [txn-id events]
  (doseq [event (filter sender-is-own-user? events)]
    (process-event! event))
  {:status 200
   :body {:txn-id txn-id}})
