(ns keanu-analytics.ingest
  (:require
   [cheshire.core :refer [generate-string parse-string]]
   [clojure.tools.logging :as log]
   [java-time :as jt]
   [conman.core :refer [with-transaction]]
   [keanu-analytics.db.core :as db]
   [keanu-analytics.config :refer [env]]
   [keanu-analytics.cal :as cal]
   [keanu-analytics.routes.services :refer [process-event!]]
   [keanu-analytics.syndb :as syndb]
   ))

(defn process-row! [row]
  (-> row
      :json
      (parse-string true)
      (process-event!)
      ))

(defn process-backlog-for-period! [from-date]
  (let [batch-size 10000
        ts-lower (cal/adate->ts from-date)
        ts-upper (cal/adate->ts (cal/next-week from-date))
        num-events (:count (first (syndb/count-message-events syndb/*synapse*
                                                        {:sender-like (str "%:" (:hs-url env))
                                                         :ts-lower ts-lower
                                                         :ts-upper ts-upper
                                                         })))
        ]
    (println "PROCESSING PERIOD" (cal/period-for from-date) ": " num-events "total events," "from" ts-lower "to" ts-upper)
    (loop [last-ts nil
           offset 0
           safety 0]
      (if (< safety 100000)
        (let [rs (syndb/get-message-events syndb/*synapse*
                                     {
                                      :sender-like (str "%:" (:hs-url env))
                                      :ts-lower ts-lower
                                      :ts-upper ts-upper
                                      :limit batch-size
                                      :offset offset
                                      })]
          (println "batch:" (int (Math/ceil (/ offset batch-size))) "total:" (int (Math/ceil (/ num-events batch-size))) "safety:" safety "batch-size:" batch-size "offset:" offset "of total" num-events)
          (if (not (empty? rs))
            (do
              (with-transaction [db/*db*]
                (doseq [row rs] (process-row! row)))
              (recur (:origin_server_ts (last rs))
                     (+ offset batch-size)
                     (inc safety)))
            last-ts))
        last-ts))))

(defn date-range
  "Returns a lazy seq of adates [from,to)"
  [from to]
  (take-while #(jt/before? % to) (iterate cal/next-week from)))

(defn ingest-from-date [start-adate end-adate]
  "Ingests events starting from `start-adate` up to and NOT including `end-adate`"
  (doseq [from (date-range start-adate end-adate)]
    (process-backlog-for-period! from)))

(comment
  ;; (def start-date (cal/from-period [2019 1]))
  (def start-date (cal/from-period [2020 1]))
  (def end-date (cal/from-period [2020 10]))

  (ingest-from-date start-date end-date)
  )
