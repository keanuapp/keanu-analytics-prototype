(ns keanu-analytics.bloom
  (:require
   [mount.core :as mount]
   [keanu-analytics.db.core :as db]
   [keanu-analytics.config :refer [env]]
   [keanu-analytics.util :refer [fmapk]]
   [clojure.java.io                 :as io]
   [caesium.crypto.shorthash :as sh]
   [caesium.util :refer [hexify unhexify]]

   [com.github.kyleburton.clj-bloom.io :as bfio]
   [com.github.kyleburton.clj-bloom :as bf])
  (:import
   [java.util BitSet]))

(comment
  (def ^{:dynamic true} words-file           "/usr/share/dict/words")
  (def ^{:dynamic true} num-expected-entries 2875518)

  (def all-words (memoize (fn [words-file]
                            (with-open [rdr (io/reader words-file)]
                              (doall (line-seq rdr))))))

  (defn add-words-to-filter! [filter]
    (dorun
     (doseq [word (all-words words-file)]
       (bf/add! filter (.toLowerCase ^String word)))))

  (defn run [hash-fn]
    (let [filter (bf/bloom-filter (* 10 1024 1024) hash-fn)]
      (add-words-to-filter! filter)
      (dorun
       (doseq [w (.split "The quick brown ornithopter hyper-jumped over the lazy trollusk" "\\s+")]
         (if (bf/include? filter (.toLowerCase ^String w))
           (prn (format "HIT:  '%s' in the filter" w))
           (prn (format "MISS: '%s' not in the filter" w)))))))

  (defn make-words-filter [m-expected-entries k-hashes hash-fn]
    (let [flt (bf/bloom-filter
               m-expected-entries
               (bf/make-permuted-hash-fn
                (or hash-fn bf/make-hash-fn-hash-code)
                (range 0 k-hashes)
                ;; (map str (range 0 k-hashes))
                ))]
      (add-words-to-filter! flt)
      flt))

  (defn words-fp-rate [count flt]
    (loop [times count
           fps   0]
      (cond (= 0 times)
            fps
            (bf/include? flt (str times))
            (recur (dec times)
                   (inc fps))

            :else
            (recur (dec times)
                   fps))))

  (defn report-fp-rate [count flt]
    (let [rate (words-fp-rate count flt)]
      (/ rate count 1.0)))

  (defn eprintf [& args]
    (.println System/err (apply format args)))

  (defn hashit [v k]
    (hexify (sh/shorthash (.getBytes v) k)))
  (def k (sh/keygen!))
  (comment
    (hashit "hello" k))

  (defn
    ^{:doc "Creates a sha1 hash function, with `x' as a constnat added to the hashed string."
      :added "1.0.0"}
    make-hash-fn-sip [#^String x]
    (let [k (sh/keygen!)]
      (fn [#^String s bits]
        (.longValue
         (.mod (java.math.BigInteger. 1 (.array (sh/shorthash (.getBytes (str s x)) k)))
               (java.math.BigInteger/valueOf bits))))))

  (defn bb->long
    ([buf] (.longValue (java.math.BigInteger. 1 (.array buf))))
    ([buf bits]
     (.longValue
      (.mod (java.math.BigInteger. 1 (.array buf))
            (java.math.BigInteger/valueOf bits)))))

  (defn make-hash-fn-sip [salt k]
    (fn [s bits]
      (-> (str s salt)
          (.getBytes)
          (sh/shorthash k)
          (bb->long bits))))
  (defn
    ^{:doc "Creates a sha1 hash function, with `x' as a constnat added to the hashed string."
      :added "1.0.0"}
    make-hash-fn-sip2 [#^String salt]
    (let [k1 (sh/keygen!)]
      (make-hash-fn-sip salt k1)))

  (defn bi [i]
    (java.math.BigInteger/valueOf i))

  (defn gi [i h1 h2]
    (fn [s bits]
      (let [hx1 (h1 s)
            hx2 (h2 s)
            m (bi bits)]
        (mod (+ hx1 (* i hx2)) m))))

  (defn make-sip-fn-trick [k1 k2]
    (let [h1 (fn [x] (bb->long (sh/shorthash (.getBytes x) k1)))
          h2 (fn [x] (bb->long (sh/shorthash (.getBytes x) k2)))
          g (fn [i] (gi (bi i) h1 h2))]
      g))

  (defn make-sip
    [k]
    (bf/make-permuted-hash-fn make-hash-fn-sip2 (map str (range 0 k))))

  (defn make-sip2
    [salts]
    (bf/make-permuted-hash-fn make-hash-fn-sip2 salts))

  (defn make-sip-trick [k k1 k2]
    (let [gi (make-sip-fn-trick k1 k2)
          hash-fns (map gi (range 0 k))]
      (fn [s bits]
        (map #(% s bits) hash-fns))))

  (def word-flt-1pct          (make-words-filter num-expected-entries 7 bf/make-hash-fn-hash-code))
  (def word-flt-crc32-1pct    (make-words-filter num-expected-entries 7 bf/make-hash-fn-crc32))
  (def word-flt-adler32-1pct  (make-words-filter num-expected-entries 7 bf/make-hash-fn-adler32))
  (def word-flt-md5-1pct      (make-words-filter num-expected-entries 7 bf/make-hash-fn-md5))
  (def word-flt-sha1-1pct     (make-words-filter num-expected-entries 7 bf/make-hash-fn-sha1))
  (def word-flt-sip-1pct     (make-words-filter num-expected-entries 3 make-hash-fn-sip2))
  (def key1 (sh/keygen!))
  (def key2 (sh/keygen!))
  (def word-flt-sip-trick (make-words-filter num-expected-entries 7 (make-sip-fn-trick key1 key2)))

  (defn main [& args]
    (let [words-file (or (first args)
                         words-file)]
      (binding [words-file (or (first args)
                               words-file)
                num-expected-entries (count (all-words words-file))]
        (eprintf "words-file=%s, num-expected-entries=%d" words-file num-expected-entries)
        (eprintf "bf/make-hash-fn-hash-code")
        (eprintf "n=%s, k=10, p=0.01: Java's hashCode: fp=%f  cardinality=%d\n"
                 num-expected-entries
                 (report-fp-rate 100000 word-flt-1pct)
                 (.cardinality ^BitSet (:bitarray word-flt-1pct)))
        (eprintf "bf/make-hash-fn-crc32")
        (eprintf "n=%s, k=10, p=0.01: CRC32:           fp=%f  cardinality=%d\n"
                 num-expected-entries
                 (report-fp-rate 100000 word-flt-crc32-1pct)
                 (.cardinality ^BitSet (:bitarray word-flt-crc32-1pct)))
        (eprintf "bf/make-hash-fn-adler32")
        (eprintf "n=%s, k=10, p=0.01: Adler32:         fp=%f  cardinality=%d\n"
                 num-expected-entries
                 (report-fp-rate 100000 word-flt-adler32-1pct)
                 (.cardinality ^BitSet (:bitarray word-flt-adler32-1pct)))
        (eprintf "bf/make-hash-fn-md5")
        (eprintf "n=%s, k=10, p=0.01: MD5:             fp=%f  cardinality=%d\n"
                 num-expected-entries
                 (report-fp-rate 100000 word-flt-md5-1pct)
                 (.cardinality ^BitSet (:bitarray word-flt-md5-1pct)))
        (eprintf "bf/make-hash-fn-sha1")
        (eprintf "n=%s, k=10, p=0.01: SHA1:            fp=%f  cardinality=%d\n"
                 num-expected-entries
                 (report-fp-rate 100000 word-flt-sha1-1pct)
                 (.cardinality ^BitSet (:bitarray word-flt-sha1-1pct)))
        (eprintf "bf/make-hash-fn-sip")
        (eprintf "n=%s, k=10, p=0.01: SipHash:         fp=%f  cardinality=%d\n"
                 num-expected-entries
                 (report-fp-rate 100000 word-flt-sip-1pct)
                 (.cardinality ^BitSet (:bitarray word-flt-sip-1pct)))
        (eprintf "bf/make-hash-fn-sip-trick")
        (eprintf "n=%s, k=10, p=0.01: SipHash:         fp=%f  cardinality=%d\n"
                 num-expected-entries
                 (report-fp-rate 100000 word-flt-sip-trick)
                 (.cardinality ^BitSet (:bitarray word-flt-sip-trick))))))

  (comment
    (main))

  (def names1 "test/fixtures/sorted/half_million.txt")
  (def names2 "test/fixtures/sorted/million_half.txt")

  (def unique-to-names1 "test/fixtures/sorted/not-in-millionhalf.txt")
  (def unique-to-names2 "test/fixtures/sorted/not-in-half.txt")

  (defn add-names-to-filter! [names-file filter]
    (dorun
     (doseq [word (all-words names-file)]
       (bf/add! filter (.toLowerCase ^String word)))))

  (defn make-names-filter [names-file m-expected-entries k-hashes hash-fn]
    (let [flt (bf/bloom-filter
               m-expected-entries
               (bf/make-permuted-hash-fn
                (or hash-fn bf/make-hash-fn-hash-code)
                (range 0 k-hashes)
                ;; (map str (range 0 k-hashes))
                ))]
      (add-names-to-filter! names-file flt)
      flt))

  (defn names-fp-rate [other-names flt]
    (loop [fps   0
           name (first other-names)
           other-names (rest other-names)]
      (cond (nil? name) fps
            (bf/include? flt name) (do (println "purports to include: " name " hash:" ((:hash-fn flt) name (:num-bits flt))) (recur
                                                                                                                              (inc fps)
                                                                                                                              (first other-names)
                                                                                                                              (rest other-names)))

            :else
            (recur
             fps
             (first other-names)
             (rest other-names)))))

  (defn report-fp-rate-names [flt]
    (let [other-names (all-words unique-to-names2)
          count (count other-names)
          rate (names-fp-rate other-names flt)]
      (/ rate count 1.0)))

  (def names-file names1)
  (def num-expected-names (* 2 (count (all-words names-file))))
  (def n-and-k (bf/optimal-n-and-k num-expected-names 0.01))
  (def names-flt-1pct (make-names-filter names-file (first n-and-k) (second n-and-k) bf/make-hash-fn-hash-code))
  (def names-flt-crc32-1pct (make-names-filter names-file (first n-and-k) (second n-and-k) bf/make-hash-fn-crc32))
  (def names-flt-adler32-1pct (make-names-filter names-file (first n-and-k) (second n-and-k) bf/make-hash-fn-adler32))
  (def names-flt-md5-1pct (make-names-filter names-file (first n-and-k) (second n-and-k) bf/make-hash-fn-md5))
  (def names-flt-sha1-1pct (make-names-filter names-file (first n-and-k) (second n-and-k) bf/make-hash-fn-sha1))
  (def names-flt-sip-trick (make-names-filter names-file (first n-and-k) (second n-and-k)  (make-sip-fn-trick key1 key2)))

  (defn compare-names []
    (let [num-expected-entries (count (all-words names-file))]
      (eprintf "names-file=%s\nnum-entries=%d" names-file num-expected-entries)

      (eprintf "bf/make-hash-fn-hash-code")
      (eprintf "n=%s, k=%s, p=0.01: Java's hashCode: fp=%f%%   cardinality=%d\n"
               (first n-and-k) (second n-and-k)
               (* 100  (report-fp-rate-names names-flt-1pct))
               (.cardinality ^BitSet (:bitarray names-flt-1pct)))
      (eprintf "bf/make-hash-fn-crc32")
      (eprintf "n=%s, k=%s, p=0.01: CRC32:           fp=%f%%   cardinality=%d\n"
               (first n-and-k) (second n-and-k)
               (* 100 (report-fp-rate-names names-flt-crc32-1pct))
               (.cardinality ^BitSet (:bitarray names-flt-crc32-1pct)))
      (eprintf "bf/make-hash-fn-sha1")
      (eprintf "n=%s, k=%s, p=0.01: SHA1:            fp=%f%%   cardinality=%d\n"
               (first n-and-k) (second n-and-k)
               (* 100 (report-fp-rate-names names-flt-sha1-1pct))
               (.cardinality ^BitSet (:bitarray names-flt-sha1-1pct)))
      (eprintf "bf/make-hash-fn-sip")
      (eprintf "n=%s, k=%s, p=0.01: SIP:            fp=%f%%  cardinality=%d\n"
               (first n-and-k) (second n-and-k)
               (* 100  (report-fp-rate-names names-flt-sip-trick))
               (.cardinality ^BitSet (:bitarray names-flt-sip-trick)))))

  (comment (compare-names)))

(defn bb->long
  "Convert a ByteBuffer to a long value"
  ([buf] (.longValue (java.math.BigInteger. 1 (.array buf)))))

(defn gi
  "Returns a function gi(x, m) = (h1(x) + ih2(x)) mod m

  h1 = first hash function
  h2 = seconds hash function
  x = the value being hashed
  i = ranges from 0 to k-1, where k is the num of hash functions
  m = size of the hash table

  see Kirsch, Adam & Mitzenmacher, Michael. (2006). Less Hashing, Same Performance: Building a Better Bloom Filter.. 4168. 456-467.
  "

  [i h1 h2]
  (fn [x m]
    (let [hx1 (h1 x)
          hx2 (h2 x)]
      (mod (+ hx1 (* i hx2))
           (biginteger m)))))

(defn make-sip-hash-fn
  "Creates a hash-function factory of type (i) -> (x,m) -> hash

  The two underlying hash functions use key1 and key2 respectively as per the Kirsch&Mitzenmacher technique"
  [key1 key2]
  (let [h1 (fn [x] (bb->long (sh/shorthash (.getBytes x) key1)))
        h2 (fn [x] (bb->long (sh/shorthash (.getBytes x) key2)))
        g (fn [i] (gi (biginteger i) h1 h2))]
    g))

(defn make-sip
  "Create `k' permuted hash functions using two siphash functions seeded with key1 and key2

  The hash functions are combined using the Kirsch&Mitzenmacher technique"
  [k key1 key2]
  (bf/make-permuted-hash-fn (make-sip-hash-fn key1 key2) (range 0 k)))

(defn serialize-filter
  "Serializes a bloom filter, returns a byte array"
  [bf]
  (let [buff (java.io.ByteArrayOutputStream. (/ (:num-bits bf) 8))]
    (with-open [oos (java.io.ObjectOutputStream. buff)]
      (bfio/filter->object-output-stream bf oos)
      (.writeObject oos (:k bf)))
    (.toByteArray buff)))

(defn deserialize-filter
  "Accepts a byte array, returns deserialized bloom filter without the hash-fn"
  [bytes]
  (with-open [ois (java.io.ObjectInputStream.
                   (java.io.ByteArrayInputStream. bytes))]
    (let [bf (bfio/object-input-stream->filter ois)
          k (.readObject ois)]
      (assoc bf :k k))))

(defn load-key-from-hex [hex]
  (println hex)
  (try
    (unhexify hex)
    (catch Exception e
      (throw (Exception. "Failed to load bloom siphash key. Is it a valid siphash key encoded in hex?")))))

(defn make-optimal-filter
  "Our own version of make-optimal-filter from clj-bloom that stores k inside the bloom map"
  [entries prob & [hash-fn]]
  (let [[m k] (bf/optimal-n-and-k entries prob)]
    (assoc
     (bf/bloom-filter
      m
      (bf/make-permuted-hash-fn
       hash-fn
       (map str (range 0 k))))
     :k k)))

(defn create-filter
  "Creates a Bloom Filter backed by two siphash hashes seeded

  The siphashes are seeded with key1 key2 respectively, and the bloom filter has parameters m and k
  defined optimally given num-entries and the accepted false positive probability."
  [num-entries prob key1 key2]
  (make-optimal-filter num-entries prob (make-sip-hash-fn key1 key2)))

(defn create-filter!
  [num-entries prob]
  (create-filter num-entries prob
                 (load-key-from-hex (env :siphash-key1))
                 (load-key-from-hex (env :siphash-key2))))

(defn create-filter-for-type! [type]
  (let [estimated-size  (get (env :bloom-filter-sizes) type)
        prob (env :probability-false-positives)]
    (create-filter! estimated-size prob)))

;; these are the types of bloom filters we use
(def bloom-filter-types [::user-engage-first ; long-lived filter recording first user engagements
                         ::user-engage-period ; period-lived filter recording period engagements
                         ::group-engage-period ; period-lived filter recording group engagements
                         ])

(defn load-bloom!
  [bloom-row hash-fn-factory]
  (println "got some bloom" bloom-row)
  (let [bf (deserialize-filter (:bytes bloom-row))
        k (:k bf)]
    (assoc bf :hash-fn
           (bf/make-permuted-hash-fn  hash-fn-factory (range 0 k)))))

(defn load-or-create-bloom!
  [type hash-fn-factory]
  (if-let [bloom-row (db/get-bloom {:id (str type)})]
    (load-bloom! bloom-row hash-fn-factory)
    (create-filter-for-type! type)))

(defn load-blooms!
  "Loads a map of bloom filters keyed by `bloom-filter-types` from the database, or creates them new if necessary.

  Returns a map with `bloom-filter-types` as the keys and their bloom filters as the values

  The clj-bloom library doesn't remember the hash-fn when roudtripped via serialization, so we need to
  re-assoc the hash-fn after loading the filter's data.
  "
  []
  (when (or (empty? (env :siphash-key1)) (empty? (env :siphash-key2)))
    (throw (Exception. "Error: config siphash-key1 and siphash-key2 are not defined")))
  (let [hash-fn-factory (make-sip-hash-fn  (load-key-from-hex (env :siphash-key1))  (load-key-from-hex (env :siphash-key2)))]
    (->> bloom-filter-types
         (map #(load-or-create-bloom! % hash-fn-factory))
         (zipmap bloom-filter-types))))

(defn- save-bloom! [type bloom]
  (db/upsert-bloom! {:id (str type) :bytes (serialize-filter bloom)}))

(defn- save-blooms! [blooms]
  (doseq [filter-type bloom-filter-types]
    (save-bloom! filter-type (blooms filter-type))))

;; our state is our bloom filters, they are stored in a duratom. this allows
;; us to keep them in memory for operations, while ensuring their durable stored in postgres in case of server restarts etc.
;; e.g., (get blooms ::user-engage-first)
(mount/defstate blooms
  :start (load-blooms!)
  :stop (save-blooms! blooms))


(defn included? [filter-type mxid]
  (bf/include? (blooms filter-type) mxid))

(def save-atom (atom (System/currentTimeMillis)))

(defn maybe-call [f & args]
  (let [now (System/currentTimeMillis)
        then @save-atom]
    (when (> (- now then) 10000)
      (apply f args)
      (reset! save-atom now))))

(defn add-mxid! [filter-type mxid]
  (let [bloom-bitset (get-in blooms [filter-type :bitarray])]
    (locking bloom-bitset
      (bf/add! (blooms filter-type) mxid)
      ;; saving the bloom on every add is not the most performant thing to do
      (maybe-call save-bloom! filter-type (blooms filter-type)))))

(def period-atom (atom nil))

(defn set-period! [period]
  (when (not= period @period-atom)
    (println "NEW PERIOD " @period-atom " -> " period)
    (.clear (get-in blooms [::user-engage-period :bitarray]))
    (reset! (get-in blooms [::user-engage-period :insertions]) 0)
    (.clear (get-in blooms [::group-engage-period :bitarray]))
    (reset! (get-in blooms [::group-engage-period :insertions]) 0)
    (save-blooms! blooms)
    (reset! period-atom period)))

(comment
  ;; how to use
  (included? :user-engage-period "hello")
  (included? :user-engage-period "dude")
  (add-mxid! :user-engage-period "dude")

  (def bf-user-p (create-filter-for-type! :user-engage-period))
  (bf/include? bf-user-p "hello")
  (bf/add! bf-user-p "hello")

  (db/create-bloom! {:id  (str :user-engage-period) :bytes (serialize-filter bf-user-p)})

  (def bf-user-p2-pre (deserialize-filter (:bytes (db/get-bloom {:id (str :user-engage-period)}))))
  (db/get-bloom {:id (str :user-engage-period2)})
  (def bf-user-p2
    (assoc bf-user-p2-pre :hash-fn (bf/make-permuted-hash-fn
                                    (make-sip-hash-fn  (load-key-from-hex (env :siphash-key1))  (load-key-from-hex (env :siphash-key2)))
                                    (range 0 (:k bf-user-p2-pre)))))

  (bf/include? bf-user-p2 "hello")
  (bf/include? bf-user-p2 "dude")
  (db/delete-bloom! {:id (str :user-engage-period)}))
