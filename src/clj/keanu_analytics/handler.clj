(ns keanu-analytics.handler
  (:require
    [clojure.tools.logging :as log]
    [keanu-analytics.middleware :as middleware]
    [keanu-analytics.routes.services :as routes]
    [keanu-analytics.middleware.formats :as formats]
    [reitit.coercion.spec :as spec-coercion]
    [reitit.swagger :as swagger]
    [reitit.swagger-ui :as swagger-ui]
    [reitit.ring :as ring]
    [ring.middleware.content-type :refer [wrap-content-type]]
    [ring.middleware.webjars :refer [wrap-webjars]]
    [ring.util.http-response :refer :all]
    [keanu-analytics.env :refer [defaults]]
    [mount.core :as mount]))

(defn txn-req [req]
;; (log/info "HELLO TRANSACTIONS")
                 (comment) (let [txn-id (get-in req [:parameters :path :txn-id])
                                events (get-in req [:parameters :body :events])]
                            (routes/receive-transaction txn-id events))
                 (ok {})
  )
(defn service-routes []
  ["/_matrix/app/v1"
   {:coercion spec-coercion/coercion
    :muuntaja formats/instance
    :swagger {:id ::api}
    :middleware (middleware/default-service-middleware)}

   ;; swagger documentation
   ["" {:no-doc true
        :swagger {:info {:title "Keanu Analytics Service"
                         :description "https://gitlab.com/keanuapp/keanu-analytics"}}}

    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
            {:url "/_matrix/app/v1/swagger.json"
             :config {:validator-url nil}})}]]

   ["/ping"
    {:get (constantly (ok {:message "pong"}))}]

   ["/transactions/:txn-id"
    {:put
     {:parameters {:path {:txn-id int?}
                   :body {:events vector?}}
      :handler txn-req
      }}]])

(mount/defstate init-app
  :start ((or (:init defaults) (fn [])))
  :stop  ((or (:stop defaults) (fn []))))

(mount/defstate app-routes
  :start
  (ring/ring-handler
    (ring/router
      [["/" {:get
             {:handler (constantly {:status 301 :headers {"Location" "/api/api-docs/index.html"}})}}]
       (service-routes)])
    (ring/routes
      (ring/create-resource-handler
        {:path "/"})
      (wrap-content-type (wrap-webjars (constantly nil)))
      (ring/create-default-handler))))

(defn app []
  (middleware/wrap-base #'app-routes))
