(ns keanu-analytics.cal
           "Functions for working with the java-ime `org.threeten.extra.AccountingChronology`.

  In this namespace all `adate`s  are instances of `org.threeten.extra.AccountingDate`s,
  and are thus local dates (i.e., lacking a time component)."
           (:require [java-time :as jt ])
           (:import
            [org.threeten.extra.chrono AccountingDate AccountingChronology AccountingChronologyBuilder AccountingYearDivision]
            [org.threeten.extra YearWeek]
            [java.time LocalDate]))
  (defn default-cal []
    (-> (AccountingChronologyBuilder.)
        (.endsOn (jt/day-of-week :sunday))
        (.inLastWeekOf (jt/month :december))
        (.withDivision AccountingYearDivision/QUARTERS_OF_PATTERN_4_4_5_WEEKS)
        (.leapWeekInMonth 12)
        (.toChronology)))

  (def ^:dynamic *calendar* (default-cal))

  (defn change-accounting-calendar! [calendar]
    (reset! *calendar* calendar))

  (defn adate
    "Creates an accounting date. "
    ([] (.dateNow *calendar*))
    ([year month day]
     (.date *calendar* year month day))
    ([local-date]
     (.date *calendar* local-date)))

  (defn now
    "Returns an accounting date for the current day."
    [] (adate))

  (defn adate->local-date
    "Converts an accounting date to a `java-time/local-date` in the ISO-8601 calendar system.
  This is the only function in this namespace that returns a non-accounting date."
    [adate]
    (LocalDate/from adate))

  (defn ts->adate
    "Converts a long value represneting the millis since the epoch to an accounting date"
    [ts]
    (-> ts
        jt/instant
        (jt/local-date-time "UTC")
        adate))

(defn adate->ts
  "Converts an accounting date into a long value representing the millish since the epoch"
  [adate]
  (->  (.atTime (adate->local-date adate) java.time.LocalTime/MIDNIGHT)
       (jt/zoned-date-time "UTC")
       (jt/instant)
       .toEpochMilli))

(defn month-length
  "Returns the number of days in the accounting date's month."
  [adate]
  (.lengthOfMonth adate))

  (defn year-length
    "Returns the number of days in the accounting date's year."
    [adate]
    (.lengthOfYear adate))

  (defn week-of-year
    "Returns the week of the accounting date's year."
    [adate]
    (jt/as adate :aligned-week-of-year))

  (defn year-week-of-year
    "Returns a tuple containing the year and week-of-year for the accounting date."
    [adate]
    (jt/as adate :year :aligned-week-of-year))

  (defn period-str
    "Returns a string formated with the year  and week-of-year.
  For example, [2019 1] would become \"2019-W1\""
    [tuple]
    (apply (partial format "%d-W%d") tuple))

(defn period-for
  [adate]
  (period-str (year-week-of-year adate))
  )
  (defn current-period
    "Returns the `year-week-of-year` tuple for the current day."
    []
    (year-week-of-year (now)))

  (defn current-period-str
    "Returns the `period-str` for the current day"
    []
    (period-str (current-period)))

(defn next-week
  "Return the adate exactly one week later"
  [a]
  (.plus a 1 (jt/unit :weeks)))


(defn from-period [[year week-of-year]]
  (-> (now)
    (.with (jt/field :year) year)
    (.with (jt/field :aligned-week-of-year) week-of-year)
    (.with (jt/field :day-of-week) 1)
   adate
   ))
