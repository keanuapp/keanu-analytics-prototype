(ns user
  "Userspace functions you can run by default in your local REPL."
  (:require
   [keanu-analytics.config :refer [env]]
   [clojure.spec.alpha :as s]
   [expound.alpha :as expound]
   [mount.core :as mount]
   [keanu-analytics.core :refer [start-app]]
   [keanu-analytics.syndb :as syndb]
   [keanu-analytics.memdb :as memdb]
   [keanu-analytics.cal :as cal]
   [keanu-analytics.ingest :as i]
   [keanu-analytics.report :as r]
   [keanu-analytics.db.core]
   [conman.core :as conman]
   [luminus-migrations.core :as migrations]
   [clojure.tools.namespace.repl :refer [refresh]]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(add-tap (bound-fn* clojure.pprint/pprint))

(defn start
  "Starts application.
  You'll usually want to run this on startup."
  []
  (mount/start-without #'keanu-analytics.core/repl-server))

(defn stop
  "Stops application."
  []
  (mount/stop-except #'keanu-analytics.core/repl-server))

(defn restart
  "Restarts application."
  []
  (stop)
  (start))

(defn restart-db
  "Restarts database."
  []
  (mount/stop #'keanu-analytics.db.core/*db*)
  (mount/start #'keanu-analytics.db.core/*db*)
  (binding [*ns* 'keanu-analytics.db.core]
    (conman/bind-connection keanu-analytics.db.core/*db* "sql/queries.sql")))

(defn reset-db
  "Resets database."
  []
  (migrations/migrate ["reset"] (select-keys env [:database-url])))

(defn migrate
  "Migrates database up for all outstanding migrations."
  []
  (migrations/migrate ["migrate"] (select-keys env [:database-url])))

(defn rollback
  "Rollback latest database migration."
  []
  (migrations/migrate ["rollback"] (select-keys env [:database-url])))

(defn create-migration
  "Create a new up and down migration file with a generated timestamp and `name`."
  [name]
  (migrations/create name (select-keys env [:database-url])))



(defn go [from to]
  (let [start-date (cal/from-period from)
        end-date (cal/from-period to)]
    (i/ingest-from-date start-date end-date)
    (r/report (i/date-range start-date end-date))
    ))


(defn do-2019 [] (go [2019 1] [2020 1]))
(defn do-2020Q1 [] (go [2020 1] [2020 14]))

(comment
  ;; (def start-date (cal/from-period [2019 1]))
  (start)
  (def start-date (cal/from-period [2019 1]))
  (def end-date (cal/from-period [2020 1]))

  (go [2019 1] [2020 1])

  (i/ingest-from-date start-date end-date)
  (r/report)
  )
