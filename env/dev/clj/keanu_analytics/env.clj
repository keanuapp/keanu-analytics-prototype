(ns keanu-analytics.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [keanu-analytics.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[keanu-analytics started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[keanu-analytics has shut down successfully]=-"))
   :middleware wrap-dev})
